const d3 = require('d3');
const _ = require('lodash');

// Example input: { edited, unedited }
const EX = {
    edited: '../pics/cat.jpg',
    unedited: '../pics/cat.jpg',
    // bounds: { width: 0, height: 0 },
    differences: [
        // NORMALIZED coordinates!
        {
            x: 0.3,
            y: 0.9,
            radius: 23,
            title: 'a floater!',
            description: '' // DEFAULT TO TITLE
        },
        {
            // ... more!
        }]
}


class CardState {
    constructor(args) {
        _.assign(this, args);

        this._found = [];
        this._remaining = this.differences;
        this._tryNumber = 0;
    }

    inferPositioning(bounds, mode) {
        const W = bounds.width,
            H = bounds.height;

        const myAspectRatio = this.aspectRatio;
        const outerAspectRatio = W / H;

        if (outerAspectRatio >= 1 && myAspectRatio > 1) {

        } else if (outerAspectRatio >= 1 && myAspectRatio < 1) {

        } else if (outerAspectRatio < 1 && myAspectRatio > 1) {

        } else if (outerAspectRatio < 1 && myAspectRatio < 1) {

        }
    }

    get aspectRatio() { return this.bounds.width / this.bounds.height; }
    get numTries() { return this._tryNumber };

    hit(x01, y01, { W, H }) {
        const x = x01 * W,
            y = y01 * H;

        const hitItem = _.find(this._remaining, (item) => {
            const rsqr = item.radius * item.radius;

            const dx = x - item.x,
                dy = y - item.y;

            return (dx * dx + dy * dy) <= rsqr;
        });

        _.remove(this._remaining, (item) => item === hitItem);

        return {
            hit: hitItem,
            remaining: this._remaining.length,
            tryNumber: ++this._tryNumber
        };
    }
}

class CardStates {
    constructor(data) {
        this._data = _.map(data, (d) => new CardState(d));

        this._completed = [];
        this._uncompleted = [];

        this._active;
        this._startTime;

        this.reset();
    }

    get cards() { return this._data; }

    get numTries() { return d3.sum(this._data, (d) => d.numTries); }

    get activeState() { return this._data[this._active]; }

    reset() {
        // _.forOwn(this._data, (d, k) => this._uncompleted.push[k])
        this._uncompleted = _.keys(this._data);
        this._completed = [];

        this._active = _.last(this._uncompleted);
        this._numTries = 0;
        this._startTime = Date.now();
    }

    _next() {
        this._completed.push(this._active);
        _.remove(this._uncompleted, (v) => v === this._active);

        this._completed.push(this._active);

        if (_.isEmpty(this._uncompleted)) return true;

        this._active = _.last(this._uncompleted);

        return false;
    }

    tryHit(x01, y01, bounds) {
        const { activeState } = this;

        const { hit, remaining, tryNumber } = activeState.hit(x01, y01, bounds);

        if (!!hit) return { hit: true, empty: this._next() };
        return { hit: false };
    }
}

class XmasCard2018 {
    constructor() {
        this.container = d3.select('#christmas-card');

        const width = window.innerWidth,
            height = window.innerHeight;

        this._bounds = { width, height };

        this._mode = 'two';
        this._modeDetailDual = { first: 'edited', last: 'unedited' };
        this._modeDetailTick = [
            { show: 'edited', time: 1000 },
            { show: 'unedited', time: 1000 },
        ];
    }

    get cards() { return this._cards; }
    get executionScheme() { return _.cloneDeep(this._modeDetailTick); }

    get bounds() { return this._bounds; };
    get cardsState() { return this._cards; }

    data(data) {
        return (this._cards = new CardStates(data), this._render(), this);
    }

    _render() {
        const { container } = this;

        if (!this.cards) return;

        const cards = container.selectAll('div.xcard')
            .data(this.cardsState.cards);

        const cardsEnter = cards
            .enter()
            .append('div')
            .attr('class', 'xcard-container');

        const cardsAll = cards.merge(cardsEnter);

        const picsEdited = cardsAll
            .selectAll('.xcard-pic.edited')
            .data(({ edited }) => [edited]);

        const picsEditedEnter = picsEdited.enter()
            .append('img')
            .attr('class', 'xcard-pic.edited')
            .attr('src', (src) => src);

        picsEdited.exit().remove();
        const allPicsEdited = picsEdited.merge(picsEditedEnter);

        const picsUnedited = cardsAll
            .selectAll('.xcard-pic.unedited')
            .data(({ unedited }) => [unedited]);

        const picsUneditedEnter = picsUnedited.enter()
            .append('img')
            .attr('class', 'xcard-pic.unedited')
            .attr('src', (src) => src);

        picsUnedited.exit().remove();

        const allPicsUnedited = picsUnedited.merge(picsUneditedEnter);


        this._selections = {
            edited: allPicsEdited,
            unedited: allPicsUnedited,
            containers: cardsAll
        };

        this._displayActive();

        // Need a way to determine whether to position them side-by-side
        // or top-bottom, use aspect ratio of pic + aspect ratio of screen!
    }

    _displayActive() {
        const active = this.cardsState.activeState;

        const { _selections } = this;
        const { edited, unedited, containers } = _selections;

        // Hide all inactive, show active and initiate the sequence on click/tap screen!

        containers.classed('xmas-show', (d) => d === active);
        const scheme = this.cardsState.executionScheme;

        // Assume it is the blink one for now!
        const myContainer = containers.filter((d) => d === active),
            myEdited = myContainer.select('.edited'),
            myUnedited = myContainer.select('.unedited');

        alert("Click OK to begin!");

        const executeStep = () => {
            if (_.isEmpty(scheme)) return;

            const item = scheme.splice(0, 1);
            const { show, time } = item;

            const showItem = show === 'edited' ? myEdited : myUnedited,
                hideItem = show === 'edited' ? myEdited : myUnedited;

            const applyHide = () => {
                    hideItem
                        .style('opacity', 0)
                        .style('transform', 'scale(0.00001)')
                },
                applyShow = () => {
                    showItem
                        .style('opacity', 1)
                        .style('transform', 'scale(1)');
                };

            hideItem.node().addEventListener('transitionend', () => {
                applyShow();
            }, { once: true, capture: true });

            showItem.node().addEventListener('transitionend', () => {
                executeStep();
            }, { once: true, capture: true });

            applyHide();
        }

        executeStep();

        alert("Find the errors!");
    }
}

window.onload = () =>
    new XmasCard2018().data([EX]);

// module.exports = XmasCard2018;
