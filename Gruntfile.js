module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browserify');
    // grunt.loadNpmTasks('grunt-contrib-uglify');
    // grunt.loadNpmTasks('grunt-contrib-cssmin');
    // grunt.loadNpmTasks('grunt-contrib-symlink');
    // grunt.loadNpmTasks('grunt-contrib-less');


    /**
     * External libs, include lib names
    /* here to have them be included in
    /* vendors.js and not app.js
    /* Cuts down compile time for app.js
     **/
    let extLibs = [
        // 'lodash',
        // 'd3'
    ];


    let now = new Date(Date.now()).toDateString();

    grunt.initConfig({
        jsdoc: {
            dist: {
                //src: ['src/*.js'],
                options: {
                    destination: 'docs',
                    configure: 'conf.json',
                    recurse: true,
                }
            }
        },
        watch: {
            options: {
                interval: 100,
                reload: true,
            },
            js: {
                files: [
                    'src/**/*.js'
                ],
                tasks: [
                    'browserify:app',
                ]
            },
            // css: {
            //     files: [
            //         './src/css/LESS/**/*.less'
            //     ],
            //     tasks: ['less'],
            //     options: {
            //         livereload: true
            //     }
            // }
        },
        // less: {
        //     dev: {
        //         options: {
        //             paths: ['./src/css/LESS/**/*.less']
        //         },
        //         files: {
        //             './src/css/app.css': './src/css/LESS/app.less'
        //         }
        //     }
        // },
        browserify: {
            app: {
                options: {
                    external: extLibs,
                    debug: true
                },
                files: {
                    'build/app.js': ['src/main.js'],
                    //    'tests/tests-bundle.js': ['tests/all-tests.js']
                }
            },
        },
    });

    grunt.registerTask('dist', ['browserify:app']);

};